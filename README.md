

## Step-by-step Installation Guide

### Virtual Environment Setup

Create a new virtual environment by choosing a Python interpreter and making a `./rasa` directory to hold it:

```
python -m venv ./rasa
```

Activate the virtual environment:

```
.\rasa\Scripts\activate
```

### Install Rasa Open Source

First make sure your `pip` version is up to date:

```
pip install -U pi
```

To install Rasa Open Source:

```
pip3 install rasa
```

When you run Rasa Open Source for the first time, you’ll see a message notifying you about anonymous usage data that is being collected. 



For some machine learning algorithms you need to install additional python packages.

If you don't mind the additional dependencies lying around, you can use

```
pip3 install rasa[full]
```

### Dependencies for spaCy

For more information on spaCy models, check out the [spaCy docs](https://spacy.io/usage/models).

You can install it with the following commands:

```
pip install rasa[spacy]
```

```
python -m spacy download en_core_web_md
```

This will install Rasa Open Source as well as spaCy and its language model for the English language, but many other languages are availabe too. We recommend using at least the "medium" sized models (`_md`) instead of the spaCy's default small `en_core_web_sm` model. Small models require less memory to run, but will likely reduce intent classification performance.

### Dependencies for MITIE

```
pip install cmake
```

```
pip install git+https://github.com/mit-nlp/MITIE.git
```

```
pip install rasa[mitie]
```

and then download the [MITIE models](https://github.com/mit-nlp/MITIE/releases/download/v0.4/MITIE-models-v0.2.tar.bz2). The file you need is `total_word_feature_extractor.dat`. Save this anywhere. If you want to use MITIE, you need to tell it where to find this file (in this example it was saved in the `data` folder of the project directory).



To initiate a rasa chatbot, run the command below in your terminal or CLI.

```
rasa init
```

This command creates a simple chatbot for a start with some sample data.

```
rasa init
┌────────────────────────────────────────────────────────────────────────────────┐
│ Rasa Open Source reports anonymous usage telemetry to help improve the product │
│ for all its users.                                                             │
│                                                                                │
│ If you'd like to opt-out, you can use `rasa telemetry disable`.                │
│ To learn more, check out https://rasa.com/docs/rasa/telemetry/telemetry.       │
└────────────────────────────────────────────────────────────────────────────────┘
Welcome to Rasa! 🤖

To get started quickly, an initial project will be created.
If you need some help, check out the documentation at https://rasa.com/docs/rasa.
Now let's start! 👇🏽

? Please enter a path where the project will be created [default: current directory]
? Directory 'C:\Projects\Django-Rasa' is not empty. Cont
inue? Yes
```





we say yes

```
Created project directory at 'C:\Users\rusla\Dropbox\23-GITHUB\Projects\Django-Rasa'.
Finished creating project structure.
? Do you want to train an initial model? 💪🏽 Yes
Training an initial model...
```

we choose yes

```
? Do you want to speak to the trained assistant on the command line? 🤖 Yes
```

we  choose yes



```
2021-08-23 14:48:22 INFO     root  - Rasa server is up and running.
Bot loaded. Type a message and press enter (use '/stop' to exit):
Your input ->  Hi
Hey! How are you?
Your input ->  /stop
```

git clone https://github.com/JiteshGaikwad/Chatbot-Widget.git

To access this chatbot as an API in your website ,run the command below in your terminal or CLI.

```
rasa run --model models --enable-api --cors "*" --debug
```



The different parameters are:
• -m: the path to the folder containing your Rasa model,
• — enable-api: enables the API
• — cors: allows the cross site request
• — debug: helps you solve errors if the chatbot does not work properly





Once you ran the command above with no errors, you command line should be as below if you are using Windows OS. To access the Rasa chatbot type the URL [http://localhost:5005](http://localhost:5005/) in our browser



![](../assets/images/posts/README/running.jpg)

Please do not close the server .



**PART 3: Integrating your Rasa chatbot with Django**

Now that we have our Rasa chatbot and its user interface, we can now integrate it with the django web framework

start your django project by running the command below:

```
django-admin startproject rasadjango .
```

This command will create a “rasadjango” folder that contains the following files

- **manage.py**: A command-line utility that lets you interact with this Django project in various ways.
- rasadjango**/__init__.py**: An empty file that tells Python that this directory should be considered as a Python package.
- rasadjango**/settings.py**: Settings/configuration for this Django project.
- rasadjango**/urls.py**: The URL declarations for this Django project; a “table of contents” of your Django-powered site.
- rasadjango**/asgi.py**: An entry-point for ASGI-compatible web servers to serve your project.
- rasadjango **/wsgi.py**: An entry-point for WSGI-compatible web servers to serve your project.



cd rasadjango

Whenever we create any app we have to add its name to the **settings.py .** we add the line

```
'rasaweb'
```

like here

```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rasaweb'
    
]
```

To add a path that will connect to the rasaweb app,

we add

```
from django.conf.urls import include, url
```

and

```
url('^$', include('rasaweb.urls')),
```

 navigate **urls.py** of the rasadjango folder and edit it as follows

```
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
urlpatterns = [
    path('admin/', admin.site.urls),
    url('^$', include('rasaweb.urls')),
]

```

Create a **urls.py** file in the rasaweb app for route the url to the **index.html** with help of views file.

![](../assets/images/posts/README/position.jpg)

```
from django.conf.urls import url
from . import views
urlpatterns = [
url('', views.index, name='index')
]
```

In the rasaweb/views.py file add following line .

```
from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'index.html')
```



copy the static folder in the rasaweb folder. create a **templates** folder the rasaweb folder and copy the index.html file 

Lastly,add following line into settings.py file

```
import os
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
```

If you have done all things perfectly run the command below from your base directory where you had created project

```
python manage.py collectstatic
```

This will take all the static files into the main directory.

Finally,

```
python manage.py runserver
```

This command will run django server which gives you url http://127.0.0.1:8000/ to interact with the chatbot . Paste this url in browser and interact with the chatbot.